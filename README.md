# ShoppingTracker

ShoppingTracker saves your shopping lists and your spending. The motivation behind its development is to track and save your visits to a grocery store, a clothing store or even a book store. You can then analyse your money spending patterns on different items across different merchants (stores). 

## How a shopping list is formed?

Every time you visit a store, you create a new shopping list and start scanning item barcodes of the items you buy. To start with, it integrates with Tesco Product/Grocery search API's. Tesco API, which is still in developer beta program, provides detailed product information including price. 

### A Big Challenge

There has been no universal single product database which can provide you product information, including price, against a barcode. Asking user to enter each individual item price (and sometimes item name and description) is not a user friendly either. So, what now?

### Scan you receipts

Yes, once you have paid at the checkout, just take a screenshot of the receipt via the app, you shopping list will be created from the items on the receipt.

# How to build / run?

Note that the project is still in development, so you may find various feature and release branches. It is suggested to checkout develop branch first. 
Once the project sets up in Android Studio, you can launch a simulator via AVD wizard or directly run it on the device connected to your computer. To build the project from command line run the following command inside your project root

./gradlew

Once it finishes, get the builds from /app/build/generated/