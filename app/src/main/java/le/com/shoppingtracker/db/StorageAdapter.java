package le.com.shoppingtracker.db;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;

public class StorageAdapter {

    public static final String KEY_CURRENCY_SYMBOL = "key_currency_symbol";

    private Context mCtx;
    private SharedPreferences mSharePrefs;

    private StorageAdapter(Context ctx) {
        mCtx = ctx;
        mSharePrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
    }

    public static StorageAdapter get(@NonNull Context ctx) {
        return new StorageAdapter(ctx);
    }

    public StorageAdapter saveAppCurrencySymbol(@NonNull String currencySymbol) {

        mSharePrefs.edit().putString(KEY_CURRENCY_SYMBOL, currencySymbol).commit();
        return this;
    }

    public String getAppCurrencySymbol() {
        return mSharePrefs.getString(KEY_CURRENCY_SYMBOL, "£");
    }

}
