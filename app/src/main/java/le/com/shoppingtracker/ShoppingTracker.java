package le.com.shoppingtracker;

import android.support.multidex.MultiDexApplication;
import android.util.Log;

import io.realm.Realm;
import io.realm.RealmConfiguration;


public class ShoppingTracker extends MultiDexApplication {

    private String TAG = "ShoppingTracker";

    @Override
    public void onCreate() {
        Log.i(TAG, "App started");
        super.onCreate();
        initRealm();

    }

    private void initRealm() {

        Realm.init(this);
        RealmConfiguration realmConfig = new RealmConfiguration.Builder()
                .name("shoppingtracker.realm").deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfig);
    }
}
