package le.com.shoppingtracker.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Utils {

    public static String getDisplayableDateTime(long timestamp) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(timestamp);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm");
        return sdf.format(c.getTime());
    }


}
