package le.com.shoppingtracker.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;

import le.com.shoppingtracker.R;

public class UiUtils {

    public static void showDialog(@NonNull Context ctx, String title, @NonNull String msg) {

        showDialog(ctx, title, msg, false);

    }

    public static void showDialog(@NonNull Context ctx, @Nullable String title, @NonNull String msg, boolean cancelable) {


        AlertDialog.Builder builder = new AlertDialog.Builder(ctx).setCancelable(false).setMessage(msg).
                setPositiveButton(ctx.getString(R.string.ok), null);

        if (!TextUtils.isEmpty(title)) {
            builder.setTitle(title);
        }

        builder.create().show();
    }

    public static void showConnectionAlertErrDialog(@NonNull Context ctx) {
        showDialog(ctx, ctx.getString(R.string.unable_to_connect), ctx.getString(R.string.internet_connection_alert));
    }

    public static void showUnknownErrDialog(@NonNull Context ctx) {
        showDialog(ctx, ctx.getString(R.string.error), ctx.getString(R.string.unknown_error));
    }

    public static void shareText(@NonNull Context ctx, @Nullable String subject, @NonNull String body) {

        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        if (!TextUtils.isEmpty(subject)) {
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
        }
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, body);
        ctx.startActivity(Intent.createChooser(sharingIntent, ctx.getResources().getString(R.string.share_using)));

    }

    public static void launchAppSettingsScreen(@NonNull Context ctx) {

        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", ctx.getPackageName(), null);
        intent.setData(uri);
        ctx.startActivity(intent);

    }


}
