package le.com.shoppingtracker.comm.data;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ProductInfo_Tesco {


    @SerializedName("products")
    public ArrayList<Product> products;

    public static class Product {

        public String gtin;
        public String tpnb;
        public String tpnc;
        public String catId;
        public String description;
        public String brand;
        public String marketingText;

    }

}
