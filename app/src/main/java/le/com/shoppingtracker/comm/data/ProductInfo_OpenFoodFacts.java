package le.com.shoppingtracker.comm.data;


import com.google.gson.annotations.SerializedName;

public class ProductInfo_OpenFoodFacts {


    @SerializedName("product")
    public Product productInfo;


    public static class Product {
        @SerializedName("product_name")
        public String productName;
        @SerializedName("categories")
        public String category;
        @SerializedName("selected_images")
        public ProductImage productImage;
    }


    public static class ProductImage {
        @SerializedName("front")
        public FrontImage frontImage;
    }

    public static class FrontImage {
        public String thumb;
        public String small;
        public String display;
    }


}
