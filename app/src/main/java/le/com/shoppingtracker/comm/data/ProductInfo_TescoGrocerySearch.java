package le.com.shoppingtracker.comm.data;

import java.util.ArrayList;

public class ProductInfo_TescoGrocerySearch {

    public Uk uk;

    public static class Uk {

        public Ghs ghs;

        public static class Ghs {

            public Products products;

            public static class Products {

                public ArrayList<Results> results;

                public static class Results {

                    public String image;
                    public long tpnb;
                    public double price;
                }
            }
        }
    }

}
