package le.com.shoppingtracker.comm;

import le.com.shoppingtracker.comm.data.ProductInfo_OpenFoodFacts;
import le.com.shoppingtracker.comm.data.ProductInfo_Tesco;
import le.com.shoppingtracker.comm.data.ProductInfo_TescoGrocerySearch;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiEndpoints {

    @GET("/api/v0/product/{barcode}.json")
    public Call<ProductInfo_OpenFoodFacts> getProductInfoFromOpenFoodFacts(@Path("name") String barcode);

    @GET("/product")
    public Call<ProductInfo_Tesco> getProductInfoFromTescoProductApi(@Query("gtin") String barcode);

    /**
     * Gets the product information via Tesco's grocery search api. Product information returned by this
     * api contains data, such as price, which is not returned by Tesco's product api. It is advised to call
     * this api alongside Tesco product api, as the app may need to display data returned from both Apis on a
     * single screen.
     *
     * @param query
     * @param offset
     * @param limit
     * @return
     */
    @GET("/grocery/products")
    public Call<ProductInfo_TescoGrocerySearch> getProductInfoFromTescoGrocerySearchApi(@Query("query") String query, @Query("offset") int offset, @Query("limit") int limit);


}
