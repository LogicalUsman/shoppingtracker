package le.com.shoppingtracker.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.mikepenz.aboutlibraries.Libs;
import com.mikepenz.aboutlibraries.LibsBuilder;

import java.util.ArrayList;
import java.util.List;

import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import le.com.shoppingtracker.BuildConfig;
import le.com.shoppingtracker.R;
import le.com.shoppingtracker.db.ShoppingList;
import le.com.shoppingtracker.util.Utils;

public class HomeActivity extends CommonActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    private String TAG = "HomeActivity";
    private RecyclerView mStoresRv;
    private StoresAdapter mStoresAdapter;
    private Realm mRealm;
    private TextView mVersionTv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Fabric.with(this, new Crashlytics());
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mStoresRv = (RecyclerView) findViewById(R.id.stores_rv);
        LinearLayoutManager llm = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mStoresRv.setLayoutManager(llm);

        mRealm = Realm.getDefaultInstance();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View header = navigationView.getHeaderView(0);
        mVersionTv = (TextView) header.findViewById(R.id.app_version_tv);
        mVersionTv.setText(String.format("%s %s", "V", BuildConfig.VERSION_NAME));

    }

    @Override
    protected void onResume() {
        super.onResume();

        String storeName = "Tesco";
        int numVisits = 0;
        String latestVisit = "";

        RealmResults<ShoppingList> items = mRealm.where(ShoppingList.class).equalTo(ShoppingList.COLUMN_STORE_ID, "tesco").
                findAllSorted(ShoppingList.COLUMN_DATE_TIME, Sort.ASCENDING);

        if (items != null && !items.isEmpty()) {
            ShoppingList item = items.last();
            storeName = item.getStoreName();
            numVisits = items.size();
            latestVisit = Utils.getDisplayableDateTime(item.getDateTime());
        }

        StoreInfo tesco = new StoreInfo();
        tesco.name = storeName;
        tesco.numVisits = numVisits;
        tesco.latestVisitDate = latestVisit;
        List<StoreInfo> storesList = new ArrayList<>(1);
        storesList.add(tesco);
        mStoresAdapter = new StoresAdapter(storesList);
        mStoresRv.setAdapter(mStoresAdapter);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        mRealm.close();
        mRealm = null;
        super.onDestroy();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_scan_barcode) {
            // Handle the camera action
        } else if (id == R.id.nav_settings) {

        } else if (id == R.id.about_this_app) {
            new LibsBuilder().withAboutVersionString(BuildConfig.VERSION_NAME).
                    withActivityTitle(getString(R.string.about_this_app)).
                    withActivityStyle(Libs.ActivityStyle.LIGHT_DARK_TOOLBAR).
                    withAboutIconShown(true).
                    withAboutVersionShown(true).
                    withAboutDescription(getString(R.string.app_description)).
                    start(this);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private class StoreInfo {
        String name;
        int numVisits;
        String latestVisitDate;
    }

    private class StoresAdapter extends RecyclerView.Adapter<StoresAdapter.VH> {


        class VH extends RecyclerView.ViewHolder {

            TextView mStoreNameTv, mNumVisitsTv, mLatestVisitDateTv;
            ConstraintLayout mRowCl;

            public VH(View v) {
                super(v);

                mRowCl = (ConstraintLayout) v.findViewById(R.id.store_row_cl);
                mStoreNameTv = (TextView) v.findViewById(R.id.store_name_tv);
                mNumVisitsTv = (TextView) v.findViewById(R.id.num_visits_tv);
                mLatestVisitDateTv = (TextView) v.findViewById(R.id.latest_visit_date_tv);

                mRowCl.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        launchShoppingListsActivity();
                    }
                });
            }
        }

        private List<StoreInfo> mStoresInfoList;

        StoresAdapter(List<StoreInfo> list) {
            mStoresInfoList = list;
        }


        @Override
        public VH onCreateViewHolder(ViewGroup parent, int viewType) {

            View v = LayoutInflater.from(HomeActivity.this).inflate(R.layout.stores_list_row, parent, false);
            return new VH(v);
        }

        @Override
        public void onBindViewHolder(VH holder, int position) {

            StoreInfo info = mStoresInfoList.get(position);
            holder.mStoreNameTv.setText(info.name);
            holder.mNumVisitsTv.setText(Integer.toString(info.numVisits));
            String date = "-";
            if (!TextUtils.isEmpty(info.latestVisitDate)) {
                date = info.latestVisitDate;
            }
            holder.mLatestVisitDateTv.setText(date);
        }

        @Override
        public int getItemCount() {
            return mStoresInfoList.size();
        }


    }

    private void launchShoppingListsActivity() {
        Intent i = new Intent(this, ShoppingHistoryActivity.class);
        startActivity(i);
    }
}
