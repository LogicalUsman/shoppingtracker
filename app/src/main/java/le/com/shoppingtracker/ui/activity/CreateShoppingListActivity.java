package le.com.shoppingtracker.ui.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.concurrent.TimeUnit;

import io.realm.Realm;
import le.com.shoppingtracker.R;
import le.com.shoppingtracker.db.ShoppingItem;
import le.com.shoppingtracker.db.ShoppingList;
import le.com.shoppingtracker.ui.EventBus;
import le.com.shoppingtracker.ui.fragment.ShoppingListFragment;
import le.com.shoppingtracker.util.UiUtils;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class CreateShoppingListActivity extends CommonActivity {

    public static final String EXTRA_DESCRIPTION = "le.com.shoppingtracker.extras_description";
    public static final String EXTRA_BRAND = "le.com.shoppingtracker.extras_brand";
    public static final String EXTRA_QUANTITY = "le.com.shoppingtracker.extras_quantity";
    public static final String EXTRA_TOTAL_PRICE = "le.com.shoppingtracker.extras_total_price";
    public static final String EXTRA_PRICE = "le.com.shoppingtracker.extras_price";
    public static final String EXTRA_BARCODE = "le.com.shoppingtracker.extras_barcode";


    private String TAG = "ShoppingListActivity";

    private FloatingActionButton mFab;
    private ShoppingListFragment mShoppingListFragment;
    private ShoppingList mShoppingList;
    private long mNextId;
    private Subscription mSubscription;
    private ConstraintLayout mShoppingListInfoCl;
    private TextView mNumItemsTv, mTotalPriceTv;
    private ShoppingListFragment.ShoppingListInfo mShoppingListInfo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_list);
        setupToolbar(getString(R.string.shopping_list));
        mFab = (FloatingActionButton) findViewById(R.id.fab);
        mShoppingListInfoCl = (ConstraintLayout) findViewById(R.id.shopping_list_info_cl);
        mNumItemsTv = (TextView) findViewById(R.id.items_tv);
        mTotalPriceTv = (TextView) findViewById(R.id.total_price_tv);

        mSubscription = EventBus.get().getObservable().subscribe((object) -> {

            if (object instanceof ShoppingListFragment.ShoppingListInfo) {
                mShoppingListInfo = (ShoppingListFragment.ShoppingListInfo) object;
                showShoppingListInfo();
            }

        });

        mShoppingListFragment = ShoppingListFragment.newInstance(null, null);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.shopping_list_container, mShoppingListFragment);
        transaction.commit();

        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchScanProductActivity();
            }
        });

        mNextId = ShoppingItem.getNextId();
        initShoppingList();
        launchScanProductActivity();

    }

    private void showShoppingListInfo() {
        mNumItemsTv.setText(String.format("%s", mShoppingListInfo.numItems));
        String price = String.format("%s %.02f", getStorageAdapter().getAppCurrencySymbol(), mShoppingListInfo.totalPrice);
        mTotalPriceTv.setText(price);
        mShoppingListInfoCl.setVisibility(View.VISIBLE);
        Observable o = Observable.timer(1000, TimeUnit.MILLISECONDS);

        o.subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).
                subscribe(new Action1() {
                    @Override
                    public void call(Object o) {
                        mShoppingListInfoCl.setVisibility(View.GONE);
                    }
                });

    }

    private void launchScanProductActivity() {
        Intent i = new Intent(this, ScanProductActivity.class);
        startActivity(i);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        String barcode = intent.getStringExtra(EXTRA_BARCODE);
        String description = intent.getStringExtra(EXTRA_DESCRIPTION);
        String brand = intent.getStringExtra(EXTRA_BRAND);
        int quantity = intent.getIntExtra(EXTRA_QUANTITY, 1);
        double price = intent.getDoubleExtra(EXTRA_TOTAL_PRICE, 0);

        ShoppingItem si = new ShoppingItem();
        si.setId(mNextId++);
        si.setItemBarcode(barcode);
        si.setItemDescription(description);
        si.setItemBrand(brand);
        si.setItemQuantity(quantity);
        si.setItemPrice(price);
        si.setStoreId(mShoppingList.getStoreId());
        si.setShoppingListId(mShoppingList.getId());

        mShoppingListFragment.addShoppingItemToList(si);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mSubscription != null) {
            mSubscription.unsubscribe();
            mSubscription = null;
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.create_shopping_list, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mShoppingListFragment.getNumShoppingItems() > 0) {

            if (item.getItemId() == R.id.action_save_list) {
                showShoppingListSaveDialog();
            } else if (item.getItemId() == R.id.action_sum) {
                showShoppingListInfo();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    private void showShoppingListSaveDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(R.string.save_your_shopping_list).setMessage(R.string.save_list_message).
                setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        saveShoppingListItems();
                    }
                }).setNegativeButton(R.string.cancel, null).setCancelable(false);

        builder.create().show();

    }

    private void saveShoppingListItems() {

        showProcessingDialog(getString(R.string.saving_your_shopping_list));
        mShoppingListFragment.saveShoppingList(new ShoppingListFragment.Callback() {

            @Override
            public void onSuccess() {
                saveShoppingList();
            }

            @Override
            public void onError(Throwable error) {
                dismissProcessingDialog();
                String msg = String.format("%s\n%s", getString(R.string.unable_to_save_shopping_list), error.getMessage());
                UiUtils.showDialog(CreateShoppingListActivity.this, getString(R.string.error), msg);
            }
        });
    }

    private void saveShoppingList() {

        Realm realm = null;

        try {
            realm = Realm.getDefaultInstance();

            realm.executeTransactionAsync(new Realm.Transaction() {

                @Override
                public void execute(Realm realm) {

                    Number n = realm.where(ShoppingItem.class).equalTo(ShoppingItem.COLUMN_SHOPPING_LIST_ID, mShoppingList.getId()).
                            findAll().sum(ShoppingItem.COLUMN_ITEM_PRICE);

                    mShoppingList.setDateTime(System.currentTimeMillis());
                    mShoppingList.setTotalPrice(n.doubleValue());
                    mShoppingList.setNumItems(mShoppingListFragment.getNumShoppingItems());

                    realm.insertOrUpdate(mShoppingList);
                }
            }, new Realm.Transaction.OnSuccess() {

                @Override
                public void onSuccess() {
                    dismissProcessingDialog();
                    launchShoppingHistoryActivity();
                }
            }, new Realm.Transaction.OnError() {

                @Override
                public void onError(Throwable error) {
                    dismissProcessingDialog();
                    String msg = String.format("%s\n%s", getString(R.string.unable_to_save_shopping_list), error.getMessage());
                    UiUtils.showDialog(CreateShoppingListActivity.this, getString(R.string.error), msg);
                }
            });
        } finally {
            realm.close();
        }

    }

    private void initShoppingList() {
        if (mShoppingList == null) {
            mShoppingList = new ShoppingList();
            mShoppingList.setId(System.currentTimeMillis());
            mShoppingList.setStoreName("Tesco");
            mShoppingList.setStoreId("tesco");
        }
    }

    private void launchShoppingHistoryActivity() {

        Intent i = new Intent(this, ShoppingHistoryActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        finish();
        startActivity(i);
    }
}
