package le.com.shoppingtracker.ui.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuItem;

import java.util.List;

import io.realm.Realm;
import io.realm.Sort;
import le.com.shoppingtracker.R;
import le.com.shoppingtracker.db.ShoppingItem;
import le.com.shoppingtracker.db.ShoppingList;
import le.com.shoppingtracker.ui.fragment.ShoppingListFragment;
import le.com.shoppingtracker.util.UiUtils;
import le.com.shoppingtracker.util.Utils;

public class ViewShoppingListActivity extends CommonActivity {

    public static final String EXTRA_SHOPPING_LIST_ID = "le.com.shoppingtracker.extra_shopping_list_id";

    private String TAG = "ViewShoppingListActivity";
    private ShoppingListFragment mShoppingListFragment;
    private long mShoppingListId;
    private List<ShoppingItem> mShoppingItems;
    private Realm mRealm;
    private int mNumItems;
    private double mTotalPrice;
    private String mStoreVisitDate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_shopping_list);

        mShoppingListId = getIntent().getLongExtra(EXTRA_SHOPPING_LIST_ID, -1);
        mRealm = Realm.getDefaultInstance();

        ShoppingList item = mRealm.where(ShoppingList.class).equalTo(ShoppingList.COLUMN_ID, mShoppingListId).findFirst();
        long timestamp = item.getDateTime();
        mTotalPrice = item.getTotalPrice();
        mNumItems = item.getNumItems();
        mStoreVisitDate = Utils.getDisplayableDateTime(timestamp);
        setupToolbar(mStoreVisitDate);
        setupFragment();

    }

    private void setupFragment() {
        mShoppingItems = mRealm.where(ShoppingItem.class).equalTo(ShoppingItem.COLUMN_SHOPPING_LIST_ID, mShoppingListId).
                findAll().sort(ShoppingItem.COLUMN_DATE_TIME, Sort.ASCENDING);

        ShoppingListFragment.Configuration configuration = new ShoppingListFragment.Configuration();
        configuration.showDeleteIcon = false;
        mShoppingListFragment = ShoppingListFragment.newInstance(mShoppingItems, configuration);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.shopping_list_container, mShoppingListFragment);
        transaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.shopping_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_delete) {
            showDeleteMessageDialog();
        } else if (id == R.id.action_info) {
            showShoppingInformation();
        }

        return super.onOptionsItemSelected(item);
    }

    private void showShoppingInformation() {
        String info = String.format(getString(R.string.shopping_list_info), mStoreVisitDate, mNumItems, getStorageAdapter().getAppCurrencySymbol(), mTotalPrice);
        UiUtils.showDialog(this, getString(R.string.shopping_list), info, false);
    }


    private void showDeleteMessageDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setCancelable(false).setTitle(R.string.delete_shopping_list).setMessage(R.string.delete_shopping_list_message).
                setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mRealm.beginTransaction();
                        mRealm.where(ShoppingList.class).equalTo(ShoppingList.COLUMN_ID, mShoppingListId).findAll().deleteAllFromRealm();
                        mRealm.commitTransaction();
                        finish();
                    }
                }).setNegativeButton(R.string.cancel, null);

        builder.create().show();
    }

    @Override
    protected void onDestroy() {
        mRealm.close();
        super.onDestroy();
    }
}
