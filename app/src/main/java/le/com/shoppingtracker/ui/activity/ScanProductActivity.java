package le.com.shoppingtracker.ui.activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.io.IOException;

import le.com.shoppingtracker.R;
import le.com.shoppingtracker.ui.AppSurfaceView;
import le.com.shoppingtracker.ui.EventBus;
import le.com.shoppingtracker.util.UiUtils;
import rx.Subscription;

/*
 * Note: It is noted that the SurfaceView holder's onSurfaceCreated(..) is not called when
 * the camera permission is prompted prior to scanBarcode() call. Note that the activity has
 * loaded the SurfaceView's view but camera source cannot start rendering frames on it because
 * camera permission needs to be granted.
 * However when scanBarcode() is called when the activity is loading (camera permission was granted)
 * the holder's callback methods work as expected. In order to fix this issue, camera source is created and starts
 * outside the holder's onSurfaceCreated() if permission was prompted, otherwise camera source starts in the
 * holder's onSurfaceCreated().
 */
public class ScanProductActivity extends CommonActivity {

    private String TAG = "ScanProductActivity";
    private ConstraintLayout mRootCl;
    private AppSurfaceView mCameraSurfaceView;
    private boolean mBarcodeDetected;
    private Subscription mSubscription;
    private CameraSource mCameraSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_product);
        setupToolbar(getString(R.string.scan_product));
        mRootCl = (ConstraintLayout) findViewById(R.id.root_layout);
        mCameraSurfaceView = (AppSurfaceView) findViewById(R.id.barcode_sv);

        // Check for the camera permission before accessing the camera.  If the
        // permission is not granted yet, request permission.
        int rc = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (rc == PackageManager.PERMISSION_GRANTED) {
            Log.i(TAG, "Permission granted.");
            scanBarcode(false);
        } else {
            requestCameraPermission();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == CAMERA_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                scanBarcode(true);
            } else {
                Snackbar.make(mRootCl, R.string.message_enable_camera_permission,
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.action_settings, view -> {
                            UiUtils.launchAppSettingsScreen(ScanProductActivity.this);
                        })
                        .show();
            }
        }

    }

    private void scanBarcode(boolean startCameraSourceOutsideHolder) {
        BarcodeDetector barcodeDetector =
                new BarcodeDetector.Builder(getApplicationContext())
                        .setBarcodeFormats(Barcode.ALL_FORMATS)
                        .build();
        if (!barcodeDetector.isOperational()) {

            StringBuilder buff = new StringBuilder(R.string.barcode_setup_error);

            IntentFilter lowStorageFilter = new IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW);
            boolean hasLowStorage = registerReceiver(null, lowStorageFilter) != null;

            if (hasLowStorage) {
                buff.append("\n");
                buff.append(R.string.low_storage_error);
                Log.e(TAG, getString(R.string.low_storage_error));
            }
            showErrorDialog(buff.toString());
            return;
        }

        CameraSource.Builder builder = new CameraSource.Builder(getApplicationContext(), barcodeDetector)
                .setFacing(CameraSource.CAMERA_FACING_BACK)
                .setRequestedPreviewSize(1600, 1024)
                .setAutoFocusEnabled(true);

        mCameraSource = builder.build();

        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {
            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {

                /* mBarcodeDetected would assure the following block of code is not called more than once.
                 * It is noticed that barcode api calls receiveDetections() multi time for the same barcode.
                 */
                if (!mBarcodeDetected && detections.getDetectedItems().size() > 0) {
                    mBarcodeDetected = true;
                    String code = detections.getDetectedItems().valueAt(0).displayValue;
                    Log.i(TAG, String.format("Detected: %s", code));
                    Intent i = new Intent(ScanProductActivity.this, ProductInfoActivity.class);
                    i.putExtra(ProductInfoActivity.EXTRA_BARCODE, code);
                    finish();
                    startActivity(i);

                }
            }
        });


        mSubscription = EventBus.get().getObservable().subscribe((o) -> {

            if (o instanceof AppSurfaceView.EventType) {

                if (o == AppSurfaceView.EventType.SURFACE_CREATED) {
                    if (!startCameraSourceOutsideHolder) {
                        startCameraSource();
                    }
                } else if (o == AppSurfaceView.EventType.SURFACE_DESTROYED) {
                    mCameraSource.stop();
                }

            }

        });

        mCameraSurfaceView.getHolder().addCallback(mCameraSurfaceView);
        if (startCameraSourceOutsideHolder) {
            startCameraSource();
        }

    }

    private void startCameraSource() {
        try {
            mCameraSource.start(mCameraSurfaceView.getHolder());
        } catch (IOException e) {
            Log.e("Error:", e.getMessage());
        } catch (SecurityException e) {
            Log.e("Error:", e.getMessage());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mSubscription != null) {
            mSubscription.unsubscribe();
        }
    }

    private void showErrorDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this).setCancelable(false).setTitle(R.string.error).
                setMessage(message).setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });

        builder.create().show();
    }
}
