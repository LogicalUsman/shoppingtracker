package le.com.shoppingtracker.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class AppSurfaceView extends SurfaceView implements SurfaceHolder.Callback {

    public static enum EventType {
        SURFACE_CREATED, SURFACE_CHANGED, SURFACE_DESTROYED;
    }

    private String TAG = "AppSurfaceView";

    public AppSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        Log.i(TAG, "Surface created.");
        EventBus.get().postEvent(EventType.SURFACE_CREATED);
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
        Log.i(TAG, "Surface changed.");
        EventBus.get().postEvent(EventType.SURFACE_CHANGED);
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        Log.i(TAG, "Surface destroyed.");
        EventBus.get().postEvent(EventType.SURFACE_DESTROYED);
    }

}
