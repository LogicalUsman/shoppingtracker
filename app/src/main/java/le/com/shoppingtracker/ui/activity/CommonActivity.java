package le.com.shoppingtracker.ui.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import le.com.shoppingtracker.R;
import le.com.shoppingtracker.db.StorageAdapter;

public class CommonActivity extends AppCompatActivity {


    public static final int CAMERA_PERMISSION = 2;

    private String TAG = "CommonActivity";
    private ProgressDialog mProcessingDialog;
    protected boolean mHomePressHandledByChildActivity;
    private StorageAdapter mStorageAdatper;


    public void showProcessingDialog(@NonNull String message) {

        mProcessingDialog = new ProgressDialog(this);
        mProcessingDialog.setMessage(message);
        mProcessingDialog.setCancelable(true);
        mProcessingDialog.show();
    }

    public void dismissProcessingDialog() {
        if (mProcessingDialog != null && mProcessingDialog.isShowing()) {
            try {
                mProcessingDialog.dismiss();
                mProcessingDialog = null;
            } catch (Exception ignored) {
            }
        }
    }


    public void setupToolbar(String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(title);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /*
         * If child of this activity wants to handle the home press (back arrow press on toolbar),
         * it can override this method and set mHomePressHandledByChildActivity to true.
         */
        if (!mHomePressHandledByChildActivity && item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onContextItemSelected(item);
    }

    public StorageAdapter getStorageAdapter() {
        if (mStorageAdatper == null) {
            mStorageAdatper = StorageAdapter.get(this);
        }
        return mStorageAdatper;
    }

    protected void requestCameraPermission() {
        final String[] permissions = new String[]{Manifest.permission.CAMERA};

        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CAMERA)) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(R.string.permission_camera_rationale).
                    setPositiveButton(R.string.ok, (dialogInterface, i) -> {
                        ActivityCompat.requestPermissions(this, permissions, CAMERA_PERMISSION);
                    });
            builder.create().show();
        } else {
            ActivityCompat.requestPermissions(this, permissions, CAMERA_PERMISSION);
        }
    }

}
