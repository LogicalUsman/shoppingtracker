package le.com.shoppingtracker.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import io.realm.Realm;
import io.realm.Sort;
import le.com.shoppingtracker.R;
import le.com.shoppingtracker.db.ShoppingList;
import le.com.shoppingtracker.util.Utils;

public class ShoppingHistoryActivity extends CommonActivity {

    private String TAG = "ShoppingHistoryActivity";

    private RecyclerView mShoppingHistoryRv;
    private TextView mEmptyListTv;
    private FloatingActionButton mFab;
    private List<ShoppingList> mShoppingList;
    private ShoppingListAdapter mShoppingListAdapter;
    private Realm mRealm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_history);
        setupToolbar(getString(R.string.shopping_history));

        mEmptyListTv = (TextView) findViewById(R.id.empty_list_tv);
        mShoppingHistoryRv = (RecyclerView) findViewById(R.id.shopping_history_rv);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        mShoppingHistoryRv.setLayoutManager(llm);
        mFab = (FloatingActionButton) findViewById(R.id.fab);

        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchCreateShoppingListActivity();
            }
        });

        mRealm = Realm.getDefaultInstance();

    }

    @Override
    protected void onResume() {

        mShoppingList = mRealm.where(ShoppingList.class).findAll().sort(ShoppingList.COLUMN_DATE_TIME, Sort.ASCENDING);
        if (mShoppingList == null || mShoppingList.isEmpty()) {
            showEmptyMessageUi(true);
        } else {
            showEmptyMessageUi(false);
            mShoppingListAdapter = new ShoppingListAdapter();
            mShoppingHistoryRv.setAdapter(mShoppingListAdapter);
        }

        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mRealm != null) {
            mRealm.close();
            mRealm = null;
        }
    }

    private void launchCreateShoppingListActivity() {
        Intent i = new Intent(this, CreateShoppingListActivity.class);
        startActivity(i);
    }

    private void showEmptyMessageUi(boolean show) {

        if (show) {
            mShoppingHistoryRv.setVisibility(View.GONE);
            mEmptyListTv.setVisibility(View.VISIBLE);
        } else {
            mShoppingHistoryRv.setVisibility(View.VISIBLE);
            mEmptyListTv.setVisibility(View.GONE);
        }
    }

    private class ShoppingListAdapter extends RecyclerView.Adapter<ShoppingListAdapter.VH> {

        @Override
        public VH onCreateViewHolder(ViewGroup parent, int viewType) {

            View v = LayoutInflater.from(ShoppingHistoryActivity.this).inflate(R.layout.shopping_history_row, parent, false);
            return new VH(v);
        }

        @Override
        public void onBindViewHolder(VH holder, int pos) {

            ShoppingList item = mShoppingList.get(pos);
            holder.mDateTimeTv.setText(Utils.getDisplayableDateTime(item.getDateTime()));
            holder.mNumItemsTv.setText(String.format("%d", item.getNumItems()));
            holder.mPriceTv.setText(String.format("%s %.02f", getStorageAdapter().getAppCurrencySymbol(), item.getTotalPrice()));

        }

        @Override
        public int getItemCount() {
            if (mShoppingList == null) {
                return 0;
            } else {
                return mShoppingList.size();
            }
        }

        class VH extends RecyclerView.ViewHolder {

            TextView mDateTimeTv, mPriceTv, mNumItemsTv;
            ConstraintLayout mRowCl;

            public VH(View v) {
                super(v);

                mRowCl = (ConstraintLayout) v.findViewById(R.id.store_row_cl);
                mDateTimeTv = (TextView) v.findViewById(R.id.date_time_tv);
                mPriceTv = (TextView) v.findViewById(R.id.price_tv);
                mNumItemsTv = (TextView) v.findViewById(R.id.items_tv);


                mRowCl.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int pos = getAdapterPosition();
                        ShoppingList item = mShoppingList.get(pos);
                        launchViewShoppingListActivity(item.getId());

                    }
                });


            }
        }

    }

    private void launchViewShoppingListActivity(long shoppingListId) {

        Intent i = new Intent(this, ViewShoppingListActivity.class);
        i.putExtra(ViewShoppingListActivity.EXTRA_SHOPPING_LIST_ID, shoppingListId);
        startActivity(i);
    }
}
