package le.com.shoppingtracker.ui.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import le.com.shoppingtracker.R;
import le.com.shoppingtracker.db.ShoppingItem;
import le.com.shoppingtracker.db.StorageAdapter;
import le.com.shoppingtracker.ui.EventBus;
import le.com.shoppingtracker.util.UiUtils;


public class ShoppingListFragment extends Fragment {

    public interface Callback {
        void onSuccess();

        void onError(Throwable error);
    }

    public static class ShoppingListInfo {
        public int numItems;
        public double totalPrice;
    }

    public static class Configuration {
        public boolean showShareIcon = true;
        public boolean showDeleteIcon = true;
    }

    private String TAG = "ShoppingListFragment";
    private RecyclerView mShoppingListRv;
    private TextView mEmptyListTv;
    private List<ShoppingItem> mShoppingItems;
    private ShoppingListAdapter mShoppingListAdapter;
    private StorageAdapter mStorageAdapter;
    private Configuration mConfiguration;
    private double mTotalPrice;

    public ShoppingListFragment() {
    }

    public static ShoppingListFragment newInstance(@Nullable List<ShoppingItem> shoppingItems, @Nullable Configuration configuration) {
        ShoppingListFragment fragment = new ShoppingListFragment();
        fragment.mShoppingItems = shoppingItems;
        if (configuration == null) {
            fragment.mConfiguration = new Configuration();
        } else {
            fragment.mConfiguration = configuration;
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_shopping_list, container, false);
        mStorageAdapter = StorageAdapter.get(getActivity());
        mShoppingListRv = (RecyclerView) v.findViewById(R.id.shopping_list_rv);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        mShoppingListRv.setLayoutManager(llm);
        mEmptyListTv = (TextView) v.findViewById(R.id.empty_list_tv);

        if (mShoppingItems == null || mShoppingItems.isEmpty()) {
            hideShoppingList(true);
        } else {
            setAdapterToList();
        }

        return v;
    }

    //region API Methods
    public void addShoppingItemToList(@NonNull ShoppingItem si) {

        if (mShoppingItems == null) {
            mShoppingItems = new ArrayList<>();
            hideShoppingList(false);
        }
        mTotalPrice += si.getItemPrice();
        mShoppingItems.add(si);
        setAdapterToList();
        postListInfoEvent();
    }

    public int getNumShoppingItems() {
        if (mShoppingItems == null) {
            return 0;
        } else {
            return mShoppingItems.size();
        }
    }

    public void saveShoppingList(@NonNull final Callback callback) {
        if (mShoppingItems != null && !mShoppingItems.isEmpty()) {
            Realm realm = null;
            try {
                realm = Realm.getDefaultInstance();
                realm.executeTransactionAsync(new Realm.Transaction() {

                    @Override
                    public void execute(Realm realm) {
                        realm.insertOrUpdate(mShoppingItems);
                    }
                }, new Realm.Transaction.OnSuccess() {

                    @Override
                    public void onSuccess() {
                        callback.onSuccess();
                    }
                }, new Realm.Transaction.OnError() {
                    @Override
                    public void onError(Throwable error) {
                        callback.onError(error);
                    }
                });
            } finally {
                realm.close();
            }


        }
    }
    //endregion


    private void hideShoppingList(boolean hide) {

        if (hide) {
            mEmptyListTv.setVisibility(View.VISIBLE);
            mShoppingListRv.setVisibility(View.GONE);
        } else {
            mEmptyListTv.setVisibility(View.GONE);
            mShoppingListRv.setVisibility(View.VISIBLE);
        }

    }

    private void setAdapterToList() {

        if (mShoppingListAdapter == null) {
            mShoppingListAdapter = new ShoppingListAdapter();
        }
        mShoppingListRv.setAdapter(mShoppingListAdapter);
    }

    private class ShoppingListAdapter extends RecyclerView.Adapter<ShoppingListAdapter.VH> {

        @Override
        public VH onCreateViewHolder(ViewGroup parent, int viewType) {

            View v = LayoutInflater.from(getActivity()).inflate(R.layout.shopping_item_row, parent, false);
            return new VH(v);
        }

        @Override
        public void onBindViewHolder(VH holder, int pos) {

            ShoppingItem item = mShoppingItems.get(pos);
            holder.mDescriptionTv.setText(item.getItemDescription());
            holder.mBrandTv.setText(item.getItemBrand());
            holder.mQuantityTv.setText(String.format("%d", item.getItemQuantity()));
            holder.mPriceTv.setText(String.format("%s %.02f", mStorageAdapter.getAppCurrencySymbol(), item.getItemPrice()));
        }

        @Override
        public int getItemCount() {

            if (mShoppingItems == null) {
                return 0;
            } else {
                return mShoppingItems.size();
            }
        }

        class VH extends RecyclerView.ViewHolder {

            TextView mDescriptionTv, mBrandTv, mQuantityTv, mPriceTv;
            ImageView mDeleteIv, mBarcodeIv, mShareIv;


            public VH(View v) {
                super(v);

                mDescriptionTv = (TextView) v.findViewById(R.id.description_tv);
                mBrandTv = (TextView) v.findViewById(R.id.brand_tv);
                mQuantityTv = (TextView) v.findViewById(R.id.quantity_tv);
                mPriceTv = (TextView) v.findViewById(R.id.price_tv);
                mDeleteIv = (ImageView) v.findViewById(R.id.delete_iv);
                mBarcodeIv = (ImageView) v.findViewById(R.id.barcode_iv);
                mShareIv = (ImageView) v.findViewById(R.id.share_iv);

                if (!mConfiguration.showDeleteIcon) {
                    mDeleteIv.setVisibility(View.GONE);
                    ((ViewGroup.MarginLayoutParams) mShareIv.getLayoutParams()).rightMargin = 0;
                }
                if (!mConfiguration.showShareIcon) {
                    mShareIv.setVisibility(View.GONE);
                }

                mBarcodeIv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int pos = getAdapterPosition();
                        UiUtils.showDialog(getActivity(), getString(R.string.barcode), mShoppingItems.get(pos).getItemBarcode());
                    }
                });

                mDeleteIv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int pos = getAdapterPosition();
                        mTotalPrice -= mShoppingItems.get(pos).getItemPrice();
                        mShoppingItems.remove(pos);
                        notifyDataSetChanged();
                        if (mShoppingItems.isEmpty()) {
                            hideShoppingList(true);
                            mShoppingItems = null;
                        } else {
                            postListInfoEvent();
                        }

                    }
                });

                mShareIv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int pos = getAdapterPosition();
                        ShoppingItem item = mShoppingItems.get(pos);
                        String msg = String.format(getString(R.string.share_shopping_item_message), item.getItemDescription(), item.getItemBrand(), item.getItemBarcode());
                        UiUtils.shareText(getActivity(), getString(R.string.my_shopping_item), msg);
                    }
                });

            }
        }


    }

    private void postListInfoEvent() {
        ShoppingListInfo info = new ShoppingListInfo();
        if (mShoppingItems != null) {
            info.numItems = mShoppingItems.size();
        }
        info.totalPrice = mTotalPrice;
        EventBus.get().postEvent(info);
    }

}
