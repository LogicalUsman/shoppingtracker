package le.com.shoppingtracker.ui.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import le.com.shoppingtracker.R;
import le.com.shoppingtracker.comm.ApiEndpoints;
import le.com.shoppingtracker.comm.RestAdapter;
import le.com.shoppingtracker.comm.data.ProductInfo_Tesco;
import le.com.shoppingtracker.comm.data.ProductInfo_TescoGrocerySearch;
import le.com.shoppingtracker.util.UiUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ProductInfoActivity extends CommonActivity {

    public static final String EXTRA_BARCODE = "le.com.extra_barcode";

    private String TAG = "ProductInfoActivity";
    private String mBarcode;
    private double mPrice, mTotalPrice;
    private String mDescription, mBrand;
    private TextView mDescriptionTv, mBrandTv, mCurrencySymbolTv;
    private EditText mPriceEt, mQuantityEt;
    private ImageButton mIncreaseQtyBtn, mDecreaseQtyBtn;
    private Button mAddToListBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_info);

        setupToolbar(getString(R.string.product_info));
        mBarcode = getIntent().getStringExtra(EXTRA_BARCODE);

        // Get views from the layout
        mDescriptionTv = (TextView) findViewById(R.id.desc_tv);
        mBrandTv = (TextView) findViewById(R.id.brand_tv);
        mPriceEt = (EditText) findViewById(R.id.price_et);
        mQuantityEt = (EditText) findViewById(R.id.quantity_et);
        mIncreaseQtyBtn = (ImageButton) findViewById(R.id.increase_iv);
        mDecreaseQtyBtn = (ImageButton) findViewById(R.id.decrease_iv);
        mAddToListBtn = (Button) findViewById(R.id.add_to_list_btn);
        mCurrencySymbolTv = (TextView) findViewById(R.id.currency_symbol_tv);
        mCurrencySymbolTv.setText(getStorageAdapter().getAppCurrencySymbol());

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int qty = Integer.parseInt(mQuantityEt.getText().toString());
                if (v == mIncreaseQtyBtn) {
                    qty += 1;
                } else {

                    if (qty > 1) {
                        qty -= 1;
                    }
                }
                mQuantityEt.setText(String.format("%d", qty));
                String price = mPriceEt.getText().toString();
                if (!TextUtils.isEmpty(price) && mPrice != 0) {
                    mTotalPrice = mPrice * qty;
                    mPriceEt.setText(String.format("%.02f", mTotalPrice));
                }

            }
        };

        mIncreaseQtyBtn.setOnClickListener(listener);
        mDecreaseQtyBtn.setOnClickListener(listener);

        mAddToListBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!TextUtils.isEmpty(mDescription)) {
                    launchCreateShoppingListActivity();
                }
            }
        });

        getProductInfo();

    }

    private void launchCreateShoppingListActivity() {
        Intent i = new Intent(this, CreateShoppingListActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        i.putExtra(CreateShoppingListActivity.EXTRA_BARCODE, mBarcode);
        i.putExtra(CreateShoppingListActivity.EXTRA_DESCRIPTION, mDescription);
        i.putExtra(CreateShoppingListActivity.EXTRA_BRAND, mBrand);
        i.putExtra(CreateShoppingListActivity.EXTRA_PRICE, mPrice);
        i.putExtra(CreateShoppingListActivity.EXTRA_TOTAL_PRICE, Double.parseDouble(mPriceEt.getText().toString()));
        i.putExtra(CreateShoppingListActivity.EXTRA_QUANTITY, Integer.parseInt(mQuantityEt.getText().toString()));
        finish();
        startActivity(i);
    }


    private void getProductInfo() {
        Retrofit retrofit = RestAdapter.createRetrofitForTescoApi(this);
        ApiEndpoints apiEndpoints = retrofit.create(ApiEndpoints.class);
        showProcessingDialog(getString(R.string.fetching_product_info));

        Call<ProductInfo_Tesco> call = apiEndpoints.getProductInfoFromTescoProductApi(mBarcode);

        call.enqueue(new Callback<ProductInfo_Tesco>() {
            @Override
            public void onResponse(Call<ProductInfo_Tesco> call, Response<ProductInfo_Tesco> response) {

                dismissProcessingDialog();
                if (!response.isSuccessful()) {
                    UiUtils.showUnknownErrDialog(ProductInfoActivity.this);
                    return;
                }

                processProductInfo(response.body());


            }

            @Override
            public void onFailure(Call<ProductInfo_Tesco> call, Throwable t) {

                dismissProcessingDialog();
                UiUtils.showConnectionAlertErrDialog(ProductInfoActivity.this);
            }
        });


    }

    private void processProductInfo(ProductInfo_Tesco productInfo) {

        if (productInfo == null || productInfo.products.isEmpty()) {
            showProductNotFoundDialog();
            return;
        }

        mDescription = productInfo.products.get(0).description;
        mBrand = productInfo.products.get(0).brand;

        mDescriptionTv.setText(mDescription);
        mBrandTv.setText(mBrand);

        // silent call
        getProductInfo(productInfo);

    }

    private void getProductInfo(final ProductInfo_Tesco productInfo) {
        Retrofit retrofit = RestAdapter.createRetrofitForTescoApi(this);
        ApiEndpoints apiEndpoints = retrofit.create(ApiEndpoints.class);
        String query = Uri.encode(productInfo.products.get(0).description);
        Call<ProductInfo_TescoGrocerySearch> call = apiEndpoints.getProductInfoFromTescoGrocerySearchApi(query, 0, 15);
        call.enqueue(new Callback<ProductInfo_TescoGrocerySearch>() {
            @Override
            public void onResponse(Call<ProductInfo_TescoGrocerySearch> call, Response<ProductInfo_TescoGrocerySearch> response) {

                dismissProcessingDialog();
                if (response.isSuccessful() && response.body() != null) {
                    displayProductInfo(productInfo, response.body());
                }
            }

            @Override
            public void onFailure(Call<ProductInfo_TescoGrocerySearch> call, Throwable t) {

                // just do nothing
            }
        });
    }

    private void displayProductInfo(ProductInfo_Tesco productInfo, ProductInfo_TescoGrocerySearch productSearchInfo) {

        if (productSearchInfo.uk.ghs.products.results.isEmpty()) {
            return;
        }

        long tpnb = 0;
        try {
            tpnb = Long.parseLong(productInfo.products.get(0).tpnb);
        } catch (NumberFormatException e) {
        }
        if (tpnb == 0) {
            return;
        }
        for (ProductInfo_TescoGrocerySearch.Uk.Ghs.Products.Results result : productSearchInfo.uk.ghs.products.results) {
            if (result.tpnb == tpnb) {

                mPrice = mTotalPrice = result.price;
                mPriceEt.setText(String.format("%.02f", result.price));
            }

        }

    }

    private void showProductNotFoundDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this).setCancelable(false).
                setTitle(R.string.product_not_found).setMessage(R.string.unable_to_find_barcode_product).
                setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        launchScanProductActivity();
                    }
                });


        builder.create().show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.mHomePressHandledByChildActivity = true;
        if (item.getItemId() == android.R.id.home) {
            launchScanProductActivity();
        }

        return super.onOptionsItemSelected(item);
    }

    private void launchScanProductActivity() {
        Intent intent = new Intent(ProductInfoActivity.this, ScanProductActivity.class);
        finish();
        startActivity(intent);
    }
}
